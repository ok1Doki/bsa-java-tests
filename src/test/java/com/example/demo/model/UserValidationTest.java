package com.example.demo.model;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class UserValidationTest {

    private Validator validator;

    @Before
    public void setUp() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Test
    public void testUserValidation_failure() {
        User user = new User(0L, "jack", "123", Collections.emptyList());
        Set<ConstraintViolation<User>> violations = validator.validate(user);
        System.out.println(violations.toString());
        assertEquals(violations.size(), 2);
    }

    @Test
    public void testUserValidation_success() {
        User user = new User(0L, "jack", "Password#$123456", Collections.emptyList());
        Set<ConstraintViolation<User>> violations = validator.validate(user);
        System.out.println(violations.toString());
        assertEquals(violations.size(), 0);
    }
}
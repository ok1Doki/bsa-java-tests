package com.example.demo.service;

import com.example.demo.exception.UserAlreadyExistsException;
import com.example.demo.model.User;
import com.example.demo.repository.ToDoRepository;
import com.example.demo.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class UserServiceTest {

    private UserRepository userRepository;

    private ToDoRepository toDoRepository;

    private UserService userService;

    @BeforeEach
    void setUp() {
        this.userRepository = mock(UserRepository.class);
        this.toDoRepository = mock(ToDoRepository.class);
        userService = new UserService(userRepository);
    }

    @Test
    void whenGetAll_thenReturnAll() {
        //mock
        var actualUsers = new ArrayList<User>();
        actualUsers.add(new User(0l, "user1", "!Password1", Collections.emptyList()));
        actualUsers.add(new User(1l, "user2", "!Password2", Collections.emptyList()));
        actualUsers.add(new User(2l, "user3", "!Password3", Collections.emptyList()));
        when(userRepository.findAll()).thenReturn(actualUsers);

        //call
        var expectedUsers = userService.findAll();

        //validate
        assertTrue(actualUsers.size() == expectedUsers.size());
        for (int i = 0; i < expectedUsers.size(); i++) {
            System.out.println(expectedUsers.get(i).toString());
            assertEquals(expectedUsers.get(i), actualUsers.get(i));
        }
    }

    @Test
    void whenSaveUserWithExistingUsername_thenThrowException() {
        //mock
        User actualUser = new User(0l, "user1", "!Password1", Collections.emptyList());
        when(userRepository.existsByUsername(actualUser.getUsername())).thenReturn(true);

        //call
        Exception exception = assertThrows(UserAlreadyExistsException.class, () -> {
            userService.save(actualUser);
        });

        //validate
        String expectedMessage = "Could not create user 'user1' already exist";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void whenCreateUserWithInvalidPassword_thenThrowException() {
        //mock
        User actualUser = new User(0l, "user1", "!Password1", Collections.emptyList());
        when(userRepository.existsByUsername(actualUser.getUsername())).thenReturn(true);

        //call
        Exception exception = assertThrows(UserAlreadyExistsException.class, () -> {
            userService.save(actualUser);
        });

        //validate
        String expectedMessage = "Could not create user 'user1' already exist";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

}

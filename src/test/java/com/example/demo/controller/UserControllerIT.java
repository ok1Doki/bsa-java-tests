package com.example.demo.controller;

import com.example.demo.model.User;
import com.google.gson.Gson;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.Map;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class UserControllerIT {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void whenCreateInvalidUser_returnBadRequest() throws Exception {
        final User invalidUser = new User(0L, "", "", Collections.emptyList());
        Gson gson = new Gson();
        this.mockMvc.perform(post("/api/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(gson.toJson(invalidUser)))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    void whenCreateUserWithExistingUsername_returnConflict() throws Exception {
        final User firstUser = new User(0L, "username", "123wersdg#R", Collections.emptyList());

        Gson gson = new Gson();
        this.mockMvc.perform(post("/api/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(gson.toJson(firstUser)))
                .andExpect(status().isOk());

        this.mockMvc.perform(post("/api/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(gson.toJson(firstUser)))
                .andDo(print())
                .andExpect(status().isConflict());
    }

    @Test
    void whenDeleteUserWithWrongPassword_returnAccessDenied() throws Exception {
        final User firstUser = new User(1L, "username", "123wersdg#R", Collections.emptyList());

        Gson gson = new Gson();
        this.mockMvc.perform(post("/api/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(gson.toJson(firstUser)))
                .andExpect(status().isOk());

        this.mockMvc.perform(delete("/api/users/1")
                .content(gson.toJson(Collections.singletonMap("password", "123123123"))))
                .andDo(print())
                .andExpect(status().isUnauthorized());
    }

}
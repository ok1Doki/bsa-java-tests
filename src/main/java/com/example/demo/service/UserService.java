package com.example.demo.service;

import com.example.demo.exception.UserAlreadyExistsException;
import com.example.demo.model.User;
import com.example.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.nio.file.AccessDeniedException;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Service
public class UserService {

    private static final String PASSWORD_REGEX =
            "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$";

    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User getOne(Long id) {
        return userRepository.getOne(id);
    }

    public User findByTodoId(Long todoId) {
        return userRepository.findByTodoId(todoId);
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }

    public void save(User user) throws UserAlreadyExistsException {
        if (userRepository.existsByUsername(user.getUsername())) {
            throw new UserAlreadyExistsException(
                    String.format("Could not create user '%s' already exist", user.getUsername()));
        }
        userRepository.save(user);
    }

    public User update(Long id, User updated) throws UserAlreadyExistsException {
        User user = userRepository.getOne(id);
        if (!Objects.equals(user.getUsername(), updated.getUsername()) &&
                userRepository.existsByUsername(updated.getUsername())) {
            throw new UserAlreadyExistsException(
                    String.format("Could not update user with id %d '%s' already exist",
                            id, updated.getUsername()));
        }
        user.setUsername(updated.getUsername());
        return userRepository.save(user);
    }

    public void delete(Long id, String password) throws AccessDeniedException {
//        if (!Pattern.compile(PASSWORD_REGEX).matcher(password).matches()) {
//            throw new ValidationException(
//                    String.format("Could not delete user with id %d, invalid password", id));
//        }
        User user = userRepository.getOne(id);

        if (!password.equals(user.getPassword())) {
            throw new AccessDeniedException(
                    String.format("Could not delete user with id %d, incorrect password", id));
        }
        user.setTodos(Collections.emptyList());
        userRepository.save(user);
        userRepository.delete(user);
    }
}

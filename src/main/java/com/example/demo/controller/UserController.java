package com.example.demo.controller;

import com.example.demo.exception.UserAlreadyExistsException;
import com.example.demo.model.ToDoEntity;
import com.example.demo.model.User;
import com.example.demo.service.ToDoService;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.nio.file.AccessDeniedException;
import java.util.List;

@RestController
@RequestMapping("/api/users")
public class UserController {

    private final ToDoService toDoService;

    private final UserService userService;

    @Autowired
    public UserController(ToDoService toDoService, UserService userService) {
        this.toDoService = toDoService;
        this.userService = userService;
    }

    @GetMapping
    public List<User> getAll() {
        return userService.findAll();
    }

    @GetMapping(value = "/{id}")
    public User getOne(@PathVariable Long id) {
        return userService.getOne(id);
    }

    @PostMapping
    public void create(@Valid @RequestBody User user) throws UserAlreadyExistsException {
        userService.save(user);
    }

    @PutMapping(value = "/{id}")
    public User update(@PathVariable Long id, @Valid @RequestBody User user) throws UserAlreadyExistsException {
        return userService.update(id, user);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id,
                       @RequestBody String password) throws AccessDeniedException {
        userService.delete(id, password);
    }

    @GetMapping(value = "/{id}/todos")
    public List<ToDoEntity> getTodos(@PathVariable Long id) {
        return toDoService.findByUserId(id);
    }

    @DeleteMapping("/{id}/todos")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteTodosForUser(@PathVariable Long id) {
        toDoService.deleteAllForUser(id);
    }
}

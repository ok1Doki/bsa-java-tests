package com.example.demo.repository;

import com.example.demo.model.ToDoEntity;

import com.example.demo.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ToDoRepository extends JpaRepository<ToDoEntity, Long> {

    @Query("select t from ToDoEntity t where t.user.id = :userId")
    List<ToDoEntity> findByUserId(@Param("userId") Long userId);
}